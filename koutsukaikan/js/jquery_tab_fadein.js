/* クリックしたときに出る点線を防ぐ（IE対策）*/
$(function(){
			$("ul.btn_tab li a").focus(function(){
				$(this).blur();
			});
})

/* タブボタンのon/offとタブの内容を切り替える */
$(function(){

	/* 初期設定 */
	$("a.selected img").attr("src",$("a.selected img").attr("src").replace(/^(.+)(\.[a-z]+)$/,"$1_on$2"));
	$(".container_tab div:not("+$("ul.btn_tab li a.selected").attr("href")+")").hide();

	/* クリック時の処理 */
	$("ul.btn_tab li a").click(function(){

		// 今のul.btn_tab li a.selectedのhrefの値を保持しておく
		var name1=$("ul.btn_tab li a.selected").attr("href");

		$("a.selected img").attr("src",$("a.selected img").attr("src").replace(/^(.+)_on(\.[a-z]+)$/,"$1$2"));
		$("ul.btn_tab li a").removeClass("selected");
		$(this).addClass("selected");

		/* クリックした画像はmouseoverで_onが付けられているので、そのままだと_on_onとなり、画像へのパスが通らなくなる。
		なので、今付いている_onをいったん削除してから再度_onを付け直す */
		$("img",this).attr("src",$("img",this).attr("src").replace(/^(.+)_on(\.[a-z]+)$/,"$1$2"));

		$("img",this).attr("src",$("img",this).attr("src").replace(/^(.+)(\.[a-z]+)$/,"$1_on$2"));

		// 保持しておいたhrefの値のdivの内容がフェードアウトしたら、クリックされたボタンのhrefの値のdivの内容をフェードインする
		$(name1).fadeOut(500,function(){

			// クリックされたul.btn_tab li a.selectedのhrefの値を保持
			var name2=$("ul.btn_tab li a.selected").attr("href");
			$(name2).fadeIn(500);
		});
		return false;
	})
})

/* マウスオーバー時の処理 */
$(function(){
	$("ul.btn_tab a").mouseover(function(){

		/* セレクタがついているかどうかの条件判断はマウスオーバーとマウスアウトのそれぞれに対して設定する。
		マウスオーバー時だけだと、クリックした後、マウスアウトするときに_onが外されて通常時の画像になってしまうため */
		var className = $(this).attr('class');
		if(className!='selected'){
			$("img",this).attr("src",$("img",this).attr("src").replace(/^(.+)(\.[a-z]+)$/, "$1_on$2"))
		}
	}).mouseout(function(){
	var className2 = $(this).attr('class');
		if(className2!='selected'){
			$("img",this).attr("src",$("img",this).attr("src").replace(/^(.+)_on(\.[a-z]+)$/,"$1$2"))
		}
	})
})