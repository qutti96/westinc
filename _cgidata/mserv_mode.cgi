#!/usr/bin/perl

#---------------------------------------------------------------------------
#	携帯用振り分けcgi
#	パーミッション：755
#	会社ID=westinc　株式会社ウエスト
#	2008.11.17	mserv2.0
#---------------------------------------------------------------------------

#-- ジャンプするＵＲＬ --#
$file_cellular = "http://m-serv.jp/westinc/";	# 携帯電話
$file_pc = "http://www.westinc.co.jp/index.html";		# ＰＣ=>HP


#========#
#　処理　#
#========#

# エージェント名取り出し
$user_agent = $ENV{'HTTP_USER_AGENT'} ;


# 上記エージェント名によって振り分け
# i-mode
if ($user_agent =~ /^DoCoMo/) {
	$file = $file_cellular;

# J-SKY
} elsif ($user_agent =~ /^J-PHONE/) {
	$file = $file_cellular;

# Vodafone
} elsif ($user_agent =~ /^Vodafone/) {
	$file = $file_cellular;

# Vodafone モトローラ端末
} elsif ($user_agent =~ /^MOT/) {
	$file = $file_cellular;

# SoftBank
} elsif ($user_agent =~ /^SoftBank/) {
	$file = $file_cellular;

# EZweb
} elsif ($user_agent =~ /^UP\.Browser/) {
	$file = $file_cellular;

# EZweb WAP2.0 対応端末
} elsif ($user_agent =~ /^KDDI/) {
	$file = $file_cellular;

# EZweb UP.Simulator
} elsif ($user_agent =~ /^UPSim/) {
	$file = $file_cellular;

# それ以外＝ＰＣ
} else {
	$file = $file_pc;
}

print "Location: $file\n\n";

exit;
